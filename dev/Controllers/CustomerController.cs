﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using dev.DAL.Dto;
using dev.DAL.Entities;
using dev.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace dev.Controllers
{ 
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork; 
        private readonly IMapper _mapper;
        public CustomerController(IUnitOfWork unitOfWork, IMapper mapper)
        { 
            _unitOfWork = unitOfWork; 
            _mapper = mapper;
        }

        [HttpGet("get")]
        public   IActionResult Get()
        { 
            var model = _unitOfWork.Customer.Get(); 
            return Json(model);
        }

        [HttpPost("add")]
        public IActionResult Add([FromBody] CustomerDto customerDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState); 

            var customer = _mapper.Map<Customer>(customerDto);
            _unitOfWork.Customer.Add(customer);
            _unitOfWork.Save();

            var model = _unitOfWork.Customer.Get(); 
            return  new OkObjectResult(model);  
        }


        [HttpPatch("update/{id}")]
        public IActionResult Update(Guid id, [FromBody] CustomerDto customerDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState); 

            var customer = _unitOfWork.Customer.Get(id); 
            if (customer == null) return new NotFoundObjectResult("Customer not found");
            if (customerDto == null) return new BadRequestObjectResult("Bad Request");

            customer.Address = customerDto.Address;
            customer.Name = customerDto.Name; 
           
            _unitOfWork.Customer.Update(customer); 
            _unitOfWork.Save();

            var model = _unitOfWork.Customer.Get(); 
            return new OkObjectResult(model); 
        }


        [HttpDelete("delete/{id}")]
        public IActionResult Delete(Guid id)
        {  
            var customer = _unitOfWork.Customer.Get(id); 
            if (customer == null) new NotFoundObjectResult("Customer not found");

            _unitOfWork.Customer.Delete(customer); 
            _unitOfWork.Save();  

            var model = _unitOfWork.Customer.Get();
            return new OkObjectResult(model);
        }
    }
}