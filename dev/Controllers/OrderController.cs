﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using dev.DAL.Dto;
using dev.DAL.Entities;
using dev.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace dev.Controllers
{ 
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly IUnitOfWork _unitOfWork; 
        private readonly IMapper _mapper;
        public OrderController(IUnitOfWork unitOfWork, IMapper mapper )
        { 
            _unitOfWork = unitOfWork; 
            _mapper = mapper;
        }

        [HttpGet("get/{id}")]
        public IActionResult Index(Guid id)
        { 
            var model = _unitOfWork.Order.Get(id); 
            return new OkObjectResult(model);
        }

        [HttpPost("add/{customerId}")]
        public IActionResult Add(Guid customerId, [FromBody] OrderDto orderDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            orderDto.Date = orderDto.GetDate();
            var order = _mapper.Map<Order>(orderDto);
            order.CustomerId = customerId; 

            _unitOfWork.Order.Add(order);
            _unitOfWork.Save(); 
          
            var model = new ResultDto()
            {
                Orders = _unitOfWork.Order.Get(order.CustomerId),
                Customers = _unitOfWork.Customer.Get()
            };
            return new OkObjectResult(model);
        }

        [HttpPatch("update/{id}")]
        public IActionResult Update(Guid id, [FromBody] OrderDto orderDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState); 

            var order = _unitOfWork.Order.GetOne(id); 
            if (order == null) return new NotFoundObjectResult("Order not found");
            if (orderDto == null) return new BadRequestObjectResult("Bad Request");
 

            order.Amount = orderDto.Amount;
            order.Number = orderDto.Number;
            order.Description = orderDto.Description;
            order.Date =   Convert.ToDateTime(orderDto.GetDate());

            _unitOfWork.Order.Update(order); 
            _unitOfWork.Save();
             
            var model = _unitOfWork.Order.Get(order.CustomerId); 
            return  new OkObjectResult(model);  
        }


        [HttpDelete("delete/{id}")]
        public IActionResult Delete(Guid id)
        {  
            var order = _unitOfWork.Order.GetOne(id); 
            if (order == null) return new NotFoundObjectResult("Order not found");

            _unitOfWork.Order.Delete(order); 
            _unitOfWork.Save();

            var model = new ResultDto()
            {
                Orders = _unitOfWork.Order.Get(order.CustomerId),
                Customers = _unitOfWork.Customer.Get()
            };
            return new OkObjectResult(model);
        }
    }
}