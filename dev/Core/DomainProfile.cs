﻿using AutoMapper;
using dev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using dev.DAL.Dto;

namespace dev.Core
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<CustomerDto, Customer>()
                .ForMember(c=>c.Id, conf=>conf.MapFrom(src => Guid.NewGuid()));

            CreateMap<OrderDto, Order>()
                .ForMember(c => c.Id, conf => conf.MapFrom(src => Guid.NewGuid()));  
        }
    }
}
