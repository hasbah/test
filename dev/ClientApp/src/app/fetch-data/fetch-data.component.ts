import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl} from '@angular/forms';
import { Message } from 'primeng/components/common/api'; 
import { MessageService } from 'primeng/components/common/messageservice';


@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  providers: [MessageService]
})
export class FetchDataComponent implements OnInit {
  public name: string;
  public customerId: string;
  public customerAdd: FormGroup;
  public customerUpdate: FormGroup;
  public orderAdd: FormGroup;
  public orderUpdate: FormGroup;

  public loading: boolean = true;
  public display: boolean = false;
  public displayOrder: boolean = false;
  public openC: boolean = false;
  public openO: boolean = false;

  public customers: Customer[];
  public customer: Customer;
  public orders: Order[];
  public order: Order;
  public url: string;
  public msgs: Message[] = [];
  public selectedRows: any[]; 

  constructor(
    private messageService: MessageService,
    private fb: FormBuilder,
    private http: HttpClient,
    @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
  }
 
  ngOnInit() {
    this.customerAdd = this.fb.group({
      'name': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(100)])),
      'address': new FormControl('', Validators.required)
    });


    this.orderAdd = this.fb.group({
      'number': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])),
      'date': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.compose([Validators.required, Validators.maxLength(100)])),
      'amount': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')]))
    });

    this.http.get<Customer[]>(this.url + 'api/customer/get')
      .subscribe(result => {
        this.customers = result;
        this.loading = false;
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  showC() {
    this.openC = !this.openC;
  }

  showO() {
    this.openO = !this.openO;
  }

  onRowSelect(event) {
    this.openC  = false;
    this.name =  event.data.name;
    this.customerId =  event.data.id;
    this.http.get<Order[]>(this.url + 'api/order/get/' + event.data.id)
      .subscribe(result => {
        this.orders = result;
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  onCustomerAdd() {
    this.loading = true;
    const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
    this.http.post<Customer[]>(this.url + 'api/customer/add', JSON.stringify(this.customerAdd.value), httpOptions)
      .subscribe(result => {
        this.customers = result;
        this.message('success', 'Success', 'Manager added');
        this.openC  = false;
        this.loading = false;
        this.customerAdd.reset();
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  updateCustomerDialog(customer: Customer) {
    this.display = true;
    this.customer = customer;
    this.customerUpdate = this.fb.group({
      'name': new FormControl(this.customer.name, Validators.required),
      'address': new FormControl(this.customer.address, Validators.required)
    });
  }

  onCustomerUpdate() {
    this.loading = true;
    const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
    this.http
      .patch<Customer[]>(this.url + 'api/customer/update/' +  this.customer.id, JSON.stringify(this.customerUpdate.value), httpOptions)
      .subscribe(result => {
        this.customers = result;

        this.message('success', 'Success', 'Manager updated');
        this.openC  = false;
        this.loading = false;
        this.display = false;
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  deleteCustomer(id: string) {
    this.loading = true;
    this.http.delete<Customer[]>(this.url + 'api/customer/delete/' + id)
      .subscribe(result => {
        this.customers = result;
        this.loading = false;
        this.message('success', 'Success', 'Manager deleted');
        this.orders = null;
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }


  onOrderAdd() {
    const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
    this.http.post<Result>(this.url + 'api/order/add/' +  this.customerId, JSON.stringify(this.orderAdd.value), httpOptions)
      .subscribe(result => {
        this.orders = result.orders;
        this.customers = result.customers;
        this.message('success', 'Success', 'Order added');
        this.openO  = false;
        this.orderAdd.reset();
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  updateOrderDialog(order: Order) {
    console.log(order);
    this.displayOrder = true;
    this.order = order;
    console.log(this.order.date);
    this.orderUpdate = this.fb.group({
      'number': new FormControl(this.order.number, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])),
      'date': new FormControl(new Date(this.order.date), Validators.required),
      'description': new FormControl(this.order.description, Validators.compose([Validators.required, Validators.maxLength(100)])),
      'amount': new FormControl(this.order.amount, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')]))
    });
  }

  onOrderUpdate() {
    const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
    this.http
      .patch<Order[]>(this.url + 'api/order/update/' +  this.order.id, JSON.stringify(this.orderUpdate.value), httpOptions)
      .subscribe(result => {
        this.orders = result;
        this.message('success', 'Success', 'Order updated');
        this.openO  = false;
        this.displayOrder = false;
    },
    result => {
      this.message('error', 'Error', result.error);
    });
  }

  deleteOrder(id: string) {
    this.http.delete<Result>(this.url + 'api/order/delete/' + id)
      .subscribe(result => {
        this.orders = result.orders;
        this.customers = result.customers;
        this.message('success', 'Success', 'Order deleted');
    }, result => {
      this.message('error', 'Error', result.error);
    });
  }

  message(severity: string, summary: string,  detail: string) {
    this.msgs.push({severity: severity, summary: summary, detail: detail});
  }
}

interface Customer {
  id: string;
  name: string;
  address: string;
  ordersCount: number;
}

interface Order {
  id: string;
  customerId: string;
  amount: string;
  number: string;
  date: string;
  description: string;
}

interface Result {
  customers: Customer[];
  orders: Order[];
}

