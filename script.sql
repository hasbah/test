USE [Master]
GO
CREATE DATABASE [dev]  
GO

USE [dev]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [uniqueidentifier] NOT NULL,
	[Number] [bigint] NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Amount] [bigint] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Customers] ([Id], [Name], [Address]) VALUES (N'00c85154-d021-4924-bda0-d677185d151e', N'Tommy Atkins', N'221B Baker Street, London ')
INSERT [dbo].[Customers] ([Id], [Name], [Address]) VALUES (N'ad0ca819-b6da-44cc-a2b1-ef80360dabeb', N'Joe Bloggs', N'1600 Pennsylvania Avenue, Washington DC')
INSERT [dbo].[Customers] ([Id], [Name], [Address]) VALUES (N'0625956d-5c91-4ef2-b8f2-f3851866d99f', N'John Doe', N'560 State Street, Brooklyn')
INSERT [dbo].[Orders] ([Id], [Number], [Date], [Amount], [Description], [CustomerId]) VALUES (N'7d9b8275-26f9-45ae-9b6d-0cef207cec8a', 2, CAST(N'2019-03-29T00:00:00.0000000' AS DateTime2), 4, N'Orange', N'0625956d-5c91-4ef2-b8f2-f3851866d99f')
INSERT [dbo].[Orders] ([Id], [Number], [Date], [Amount], [Description], [CustomerId]) VALUES (N'cd63cf96-68b6-4193-bce6-2928a3ef17ab', 1, CAST(N'2019-03-31T00:00:00.0000000' AS DateTime2), 12, N'Apple', N'ad0ca819-b6da-44cc-a2b1-ef80360dabeb')
INSERT [dbo].[Orders] ([Id], [Number], [Date], [Amount], [Description], [CustomerId]) VALUES (N'2e0816b7-1427-473c-bc75-88452538d9f7', 2, CAST(N'2019-03-25T00:00:00.0000000' AS DateTime2), 3, N'Kiwi', N'ad0ca819-b6da-44cc-a2b1-ef80360dabeb')
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customers_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers_CustomerId]
GO
