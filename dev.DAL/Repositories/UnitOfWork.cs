﻿using dev.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using dev.DAL.EF;
using Microsoft.EntityFrameworkCore;

namespace dev.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private DevContext _context;
        private bool _disposed; 

        private CustomerRepository _customerRepository;
        private OrderRepository _orderRepository;
        public UnitOfWork(DevContext context)
        {
            _context = context; 
        }

        public ICustomer Customer => _customerRepository ?? (_customerRepository = new CustomerRepository(_context));

        public IOrder Order => _orderRepository ?? (_orderRepository = new OrderRepository(_context));

        public void Save()
        { 
            _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
