﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dev.DAL.Dto;
using dev.DAL.EF;
using dev.DAL.Entities;
using dev.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace dev.DAL.Repositories
{
    public class OrderRepository : IOrder
    {
        private readonly DevContext _context;
        
        public OrderRepository(DevContext context)
        {
            _context = context;
        }

        public void Add(Order order)
        {
            _context.Entry(order).State = EntityState.Added;
        }

        public void Delete(Order order)
        {
            _context.Entry(order).State = EntityState.Deleted;
        }

        public IList<OrderDto> Get(Guid id)
        {
            return  _context.Orders
                .Where(o=>o.CustomerId == id)
                .OrderBy(o=>o.Date)
                .Select(o => new OrderDto()
                {
                    Id = o.Id,
                    Number = o.Number,
                    CustomerId = o.CustomerId,
                    Amount = o.Amount,
                    Date = o.Date.ToString("MM/dd/yyyy"),
                    Description = o.Description
                }) 
                .ToList();
        }

        public Order GetOne(Guid id)
        {
            return  _context.Orders
                .FirstOrDefault(o=>o.Id == id);
        }

        public void Update(Order order)
        {
            _context.Entry(order).State = EntityState.Modified; 
        }
    }
}
