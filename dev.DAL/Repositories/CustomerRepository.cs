﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dev.DAL.Dto;
using dev.DAL.EF;
using dev.DAL.Entities;
using dev.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace dev.DAL.Repositories
{

    public class CustomerRepository : ICustomer
    {
        private readonly DevContext _context;
        
        public CustomerRepository(DevContext context)
        {
            _context = context;
        }


        public void Add(Customer customer)
        {
            _context.Entry(customer).State = EntityState.Added;
        }

        public void Update(Customer customer)
        {   
            _context.Entry(customer).State = EntityState.Modified; 
        }

        public void Delete(Customer customer)
        {
            _context.Entry(customer).State = EntityState.Deleted;
        }

        public  IList<CustomerDto>  Get()
        {
            return   _context.Customers
                .Include(c=>c.Orders)
                .OrderBy(c=>c.Name)
                .Include(p=>p.Orders)
                .Select(c=> new CustomerDto
                {
                    Name =  c.Name,
                    Address =  c.Address,
                    Id = c.Id,
                    OrdersCount = c.Orders.Count()
                }) 
                .ToList();
        }

        public   Customer Get(Guid id)
        { 
            return _context.Customers.FirstOrDefault(c => c.Id == id);
        } 
    }
}
