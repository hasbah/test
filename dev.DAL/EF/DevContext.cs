﻿using Microsoft.EntityFrameworkCore; 
 
using dev.DAL.Entities;

namespace dev.DAL.EF
{
    public class DevContext: DbContext
    {
        public DevContext(DbContextOptions options) 
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        { 
            base.OnModelCreating(builder); 
            builder.Entity<Customer>().HasKey(m=>m.Id);   

            builder.Entity<Order>().HasKey(m=>m.Id);  
        }
    }
}
