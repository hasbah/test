﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace dev.DAL.Dto
{
   public class OrderDto
    { 
        public Guid Id { get; set; }    
         
        [Required]
        public uint Number { get; set; } 
         
        [Required]
        public string Date { get; set; }
        
        [Required]
        public uint Amount { get; set; }
         
        [Required, MaxLength(100)] 
        public string  Description { get; set; }
         
        public Guid CustomerId { get; set; }  
        
        public string GetDate()
        {
            return DateTime.ParseExact(this.Date, "MM.dd.yyyy", CultureInfo.InvariantCulture).ToShortDateString();
        }
    }
}
