﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace dev.DAL.Dto
{
    public class CustomerDto
    { 
        public Guid Id { get; set; }

        [Required, MaxLength(100)] 
        public string Name { get; set; }
         
        [Required, MaxLength(100)] 
        public string Address { get; set; } 
        public int  OrdersCount { get; set; } 
    }
}
