﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dev.DAL.Dto
{
    public class ResultDto
    {
        public IList<OrderDto> Orders  { get; set; }
        public IList<CustomerDto> Customers  { get; set; }
    }
}
