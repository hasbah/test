﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dev.DAL.Entities
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }    

        [Required] 
        public uint Number { get; set; } 

        [Required] 
        public DateTime Date { get; set; }
            
        [Required] 
        public uint Amount { get; set; }

        [Required, MaxLength(100)]
        public string  Description { get; set; } 

        [Required]
        public Guid CustomerId { get; set; } 

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
    }
}
