﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dev.DAL.Entities
{
    public class Customer
    {
        [Key] 
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required, MaxLength(100)] 
        public string Name { get; set; }

        [Required, MaxLength(100)] 
        public string Address { get; set; }
   
        public virtual ICollection<Order> Orders { get; set; }
    }
}
