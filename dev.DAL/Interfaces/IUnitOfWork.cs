﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dev.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomer Customer { get; }
        IOrder Order { get; }
        void Save();
    }
}
