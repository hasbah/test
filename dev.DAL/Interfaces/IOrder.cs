﻿using dev.DAL.Dto;
using dev.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace dev.DAL.Interfaces
{
    public interface IOrder
    {
        IList<OrderDto> Get(Guid id); 

        Order GetOne(Guid id); 

        void Add(Order order);
        void Update(Order order); 
        void Delete(Order order);
    }
}
