﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using dev.DAL.Dto;
using dev.DAL.Entities;

namespace dev.DAL.Interfaces
{
    public interface ICustomer
    { 
        IList<CustomerDto> Get(); 
        void Add(Customer customer);
        void Update(Customer customer); 
        void Delete(Customer customer); 
        Customer Get(Guid id); 
    }
}
